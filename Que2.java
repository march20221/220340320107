import java.util.*;
class Que2{}
class LinkedList{
	static Node head;
	static class Node{
		int data;
		Node next;
		
		Node(int d){
			data= d;
			next = null;
		}
	}
	
	static Node reverse (Node node){
		Node prev = null;
		Node current = node;
		Node next = null;
		
		while(current != null){
			next = current.next;
			current.next = prev;
			prev =  current ;
			current = next;
		}
		node = prev;
		return node;
	}
	
	void printList(Node node){
		while (node != null){
			System.out.print(node.data+"  ");
			node = node.next;
		}
	}
	
	public static void main(String [] args ){
		
		LinkedList ll = new LinkedList();
	    ll.head = new Node(1);
	    ll.head.next = new Node(5);
		ll.head.next.next = new Node(1);
		ll.head.next.next.next = new Node(2);
		ll.head.next.next.next.next = new Node(3);
        ll.head.next.next.next.next.next = new Node(4);
        ll.head.next.next.next.next.next.next = new Node(5);
		
		head = ll.reverse(head);
		ll.printList(head);
	}
	
}